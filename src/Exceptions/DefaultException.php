<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen\Exceptions;

use Exception;

class DefaultException extends Exception
{
    private $messageArray = [];

    public function __construct($message, $code)
    {
        $messageArray[] = "ref.$code";
        $messageArray[] = $message;
        $messageArray[] = "กลุ่มงานพัฒนาระบบสารสนเทศ สำนักวิทยบริการและเทคโนโลยีสารสนเทศ ติดต่อ 1506 หรือ 1525";
        parent::__construct(\implode(', ', $messageArray), $code);
    }   
}