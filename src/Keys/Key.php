<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen\Keys;

use NSRU\Authen\Exceptions\DefaultException;
use NSRU\Authen\Configurations;
use NSRU\Authen\Element;

class Key extends Element
{
    private $key = '';
    private $hash = '';

    public function getKey(): string
    {
        return $this->key;
    }

    protected function setKey($key): void
    {
        $this->key = $key;
    }

    protected function isKeyNotEmpty(): bool
    {
        return $this->getKey() != '';
    }

    protected function getHash(): string
    {
        return $this->hash;
    }

    protected function setHash($hash): void
    {
        $this->hash = $hash;
    }

    protected function isHashNotEmpty(): bool
    {
        return $this->getHash() != '';
    }

    public function isKeyValid(): bool
    {
        if( $this->isKeyNotEmpty() && $this->isHashNotEmpty() )
        {
            return true;
        } else {
            return false;
        }
    }
    

    protected function importFromQueryString()
    {
        if(isset($_GET['key'])) $this->setKey($_GET['key']);
        if(isset($_GET['hash'])) $this->setHash($_GET['hash']);
    }

    /**
     * @todo ข้ามขั้นตอนการตรวจสอบ Hash ไปก่อน
     */
    public function import(string $exportedText)
    {
        $json = \base64_decode($exportedText);
        $contents = \json_decode($json);
        
        $key = new MasterKey();

        if(isset($contents->key)) $key->setKey($contents->key);
        else throw new DefaultException('ไม่สามารถ Import Key ได้ เนื่องจากไม่พบ Key ในรายการที่ส่งเข้ามา', 202106161146);

        if(isset($contents->hash)) $key->setHash($contents->hash);
        else throw new DefaultException('ไม่สามารถ Import Key ได้ เนื่องจากไม่พบ Hash ในรายการที่ส่งเข้ามา', 202106161147);

        $this->setKey($key->getKey());
        $this->setHash($key->getHash());
    }

    public function export()
    {
        $exportContents = (object) [
            'key' => $this->getKey(),
            'hash' => $this->getHash()
        ];
        $json = \json_encode($exportContents);
        return \base64_encode($json);
    }
}