<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen\Keys;

use NSRU\Authen\Configurations;
use PlaygroundStudio\BlackBridge\Routers\Redirect as Redirect;

class DraftKey extends Key
{
    
    public static function create(string $thisPageUrl, bool $autoRedirect = false)
    {
        $draftKey = new DraftKey();
        $draftKey->install($thisPageUrl, $autoRedirect);
        return $draftKey;
    }
    
    /**
     * @param bool $autoRedirect 
     * @return bool 
     */
    public function install(string $thisPageUrl, bool $autoRedirect = false): bool
    {
        $this->importFromQueryString();
        if($this->isKeyValid())
        {
            return true;
        } else {
            if($autoRedirect) $this->requestNewKey($thisPageUrl);
            return false;
        }
    }

    public function requestNewKey(string $thisPageUrl): void
    {
        $url = $this->getUrlForRequestNewKey($thisPageUrl);
        Redirect::byGetMethodViaPhpHeaderLocation($url);
    }

    private function getUrlForRequestNewKey(string $thisPageUrl): string
    {
        return $this->getServiceBaseUrl("draft/" . \base64_encode($thisPageUrl));
    }
}