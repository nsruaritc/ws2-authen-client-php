<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen\Keys;

use NSRU\Authen\Exceptions\DefaultException;
use PlaygroundStudio\BlackBridge\Transporters\Curl;

class MasterKey extends Key
{
    public static function create(DraftKey $draftKey)
    {
            $masterKey = new MasterKey();

            $curl = new Curl($masterKey->getServiceBaseUrl('master'));
            $curl->withoutSSLVerify();
            $curl->withBearerToken($draftKey->getKey());
            $curl->withReturn();
            $data = (object) $curl->getJSON(true);

            if(isset($data) && isset($data->has_exception) && !$data->has_exception)
            {
                $masterKey->setKey($data->master_key);
                $masterKey->setHash($data->master_hash);
            } else {
                throw new DefaultException('DraftKey ไม่ถูกต้อง', 202106160908);
            }

            return $masterKey;
    }

    public function isKeyValid(): bool
    {
        return parent::isKeyValid();
    }
}