<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen;

trait Configurations
{
    /**
     * ที่อยู่ของ Authen Service
     */
    public function getServiceBaseUrl($uri = ""): string
    {
        $serviceUrl = "https://ws2.nsru.ac.th/client";
        //$serviceUrl = "http://ws2.nsru-dev.ac.th/client";
        if($uri != "")
        {
            return $serviceUrl . '/' . $uri;
        } else {
            return $serviceUrl;
        }
    }
}