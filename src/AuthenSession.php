<?php
/** @noinspection PhpMultipleClassDeclarationsInspection */
/** @noinspection SpellCheckingInspection */

namespace NSRU\Authen;

use Exception;
use NSRU\Authen\Element;
use NSRU\Authen\Exceptions\DefaultException;
use NSRU\Authen\Keys\MasterKey;
use PlaygroundStudio\BlackBridge\Routers\Redirect;
use PlaygroundStudio\BlackBridge\Transporters\Curl;

class AuthenSession extends Element
{
    use Configurations;

    private $key;

    public function setKey(MasterKey $masterKey)
    {
        $this->key = $masterKey;
    }

    public function getSessionStatus()
    {
        $curl = new Curl($this->getServiceBaseUrl('status'));
        $curl->withoutSSLVerify();
        $curl->withBearerToken($this->key->getKey());
        $curl->withReturn();
        $data = (object) $curl->getJSON(true);

        return $data;
    }

    public function isAlreadyLogin($sess = null): bool
    {
        if(!$sess) $sess = $this->getSessionStatus();
        if(isset($sess->is_already_login))
        {
            return $sess->is_already_login;
        } else {
            throw new DefaultException("Is Already Login Not Found", 202106161309);
        }
    }

    public function getLoginUsername($sess = null): string
    {
        if(!$sess) $sess = $this->getSessionStatus();
        if(isset($sess->is_already_login))
        {
            return $sess->username;
        } else {
            throw new DefaultException("Username Not Found", 202106161309);
        }
    }

    public function login($username, $password)
    {
        $curl = new Curl($this->getServiceBaseUrl('authentication'));
        $curl->withoutSSLVerify();
        $curl->withBearerToken($this->key->getKey());
        $curl->withReturn();
        $data = (object) $curl->postJSON([
            'username' => $username,
            'password' => $password
        ]);

        if($data->is_login_success)
        {
            return true;
        } else {
            return false;
        }
    }

    public function logout($logoutBackPageUrl)
    {
        $uri = "clear/" . \base64_encode($logoutBackPageUrl);
        $url = $this->getServiceBaseUrl($uri);
        Redirect::byGetMethodViaPhpHeaderLocation($url);
    }
}